import React, { createRef } from 'react';
import ReactDOM from 'react-dom';

import './index.css';

import sso from './sso-login';

function App() {
  const logEl = createRef();

  const log = function(...messages) {
    const msgs = messages.map(msg => JSON.stringify(msg, null, 2).replace(/^"|"$/g, '')).join(' ');
    logEl.current.innerHTML += `\n\n${new Date().toISOString()} - ${msgs}`;
    logEl.current.scrollTop = logEl.scrollHeight;
  };

  function _login() {
    sso.login()
      .then(user => log('user logged in, access_token =', user.access_token))
      .catch(err => {
        if (err instanceof sso.AuthorizationFailedError) log('authorization failed (probably a CORB issue)');
        else log('some internal error', err) || console.error(err);
      });
  }

  function _refreshLogin() {
    sso.refreshLogin()
      .then(user => {
        if (user === null) log('there is no user logged in');
        else log('user logged in, access_token =', user.access_token);
      })
      .catch(err => {
        if (err instanceof sso.AuthorizationFailedError) log('authorization failed (probably a CORB issue or oauth page requires interaction)');
        else log('some internal error', err) || console.error(err);
      });
  }

  function _check() {
    sso.check({ asynchronous: true })
      .then(user => {
        if (user === null) log('there is no user logged in with this browser');
        else log('user logged in, access_token =', user.access_token);
      });
  }

  function _syncCheck() {
    const user = sso.check({ asynchronous: false })
    if (user === null) log('there is no user logged in with this browser');
    else log('user logged in, access_token =', user.access_token);
  }

  function _logout() {
    sso.logout()
      .then(() => log('logged out from <your app>'));
  }

  function _logoutGlobally() {
    sso.logoutGlobally()
      .then(() => log('logged out from SSO entirely'));
  }

  return (
    <>
      <h1>@merl.ooo/sso-login Demo Page</h1>
      <nav>
        <button type="button" onClick={_login}>SSO.login()</button>
        <button type="button" onClick={_refreshLogin}>SSO.refreshLogin()</button>
        <button type="button" onClick={_check}>SSO.check()</button>
        <button type="button" onClick={_syncCheck}>{`SSO.check({ asynchronous: false })`}</button>
        <button type="button" onClick={_logout}>SSO.logout()</button>
        <button type="button" onClick={_logoutGlobally}>SSO.logoutGlobally()</button>
      </nav>
      <pre id="log" ref={logEl}></pre>
    </>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
