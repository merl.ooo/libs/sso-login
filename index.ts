import pTimeout, { TimeoutError } from 'p-timeout';

const wdw = global?.window || undefined;

interface Endpoint {
  method: 'POST' | 'GET' | 'PUT' | 'DELETE';
  url: string;
}

interface Endpoints {
  login: Endpoint;
  check: Endpoint;
  logout: Endpoint
}

interface Options {
  baseUrl: string;
  timeout: number;
  storage: globalThis.Storage;
  storageKey: string;
  endpoints: Endpoints;
}

const dummyStorage = new Map<string, string>();
const SessionStorageProxy = wdw?.sessionStorage || {
  getItem(key: string) {
    if (wdw?.sessionStorage) return wdw.sessionStorage.getItem(key);
    return dummyStorage.get(key) || null;
  },
  setItem(key: string, data: string) {
    if (wdw?.sessionStorage) return wdw.sessionStorage.setItem(key, data);
    dummyStorage.set(key, data);
  },
  removeItem(key: string) {
    if (wdw?.sessionStorage) return wdw.sessionStorage.removeItem(key);
    dummyStorage.delete(key);
  },
};

const defaultOptions: Options = {
  baseUrl: 'https://sso.merl.ooo',
  timeout: 5000,
  storage: SessionStorageProxy,
  storageKey: 'merl-sso-user',
  endpoints: {
    login: { method: 'GET', url: 'login' },
    check: { method: 'POST', url: 'sso' },
    logout: { method: 'DELETE', url: 'sso' },
  },
};

interface Storage {
  getItem(key: string): string | null,
  setItem(key: string, data: string): void,
  removeItem(key: string): void,
}

class Store {
  private storage: Storage;
  private key: string;
  constructor(storage: Storage, key: string) {
    this.storage = storage;
    this.key = key;
  }

  get(): object | null {
    const item = this.storage.getItem(this.key);
    return item ? JSON.parse(item) : item;
  }

  set(data: object): void {
    return this.storage.setItem(this.key, JSON.stringify(data));
  }

  remove(): void {
    return this.storage.removeItem(this.key);
  }
}

class ApiError extends Error {
  status: number;
  response?: Record<string, any>;

  constructor({ status, statusText, response }: XMLHttpRequest) {
    super(statusText || 'Unknown Server Error');
    this.status = status || 599;
    this.response = response ? JSON.parse(response) : undefined;
  }
}

class AuthorizationFailedError extends Error {
  constructor(message?: string) {
    super(message || 'Authorization failed');
  }
}

/** slightly convenient XHR requests */
function api({ method, url }: Endpoint, { baseUrl, timeout }: Options, asynchronous: false): Record<string, any>;
function api({ method, url }: Endpoint, { baseUrl, timeout }: Options, asynchronous?: true):Promise<Record<string, any>>;
function api({ method, url }: Endpoint, { baseUrl, timeout }: Options, asynchronous = true) {
  const xhr = new XMLHttpRequest();
  xhr.open(method, `${baseUrl}/${url}`, asynchronous);
  xhr.withCredentials = true;
  if (!asynchronous) {
    xhr.send(null);
    if (xhr.status < 200 || xhr.status >= 300) throw new ApiError(xhr);
    return JSON.parse(xhr.response);
  }
  xhr.timeout = timeout;
  const result = new Promise<Record<string, any>>((resolve, reject) => {
    xhr.onload = () => {
      if (xhr.readyState === 4 && xhr.status >= 200 && xhr.status < 300) resolve(JSON.parse(xhr.response));
      else reject(new ApiError(xhr));
    };
    xhr.onerror = () => reject(new ApiError(xhr));
  });
  xhr.send(null);
  return result;
}

class SSO {
  private options: Options;
  private store: Store;
  public AuthorizationFailedError;

  constructor(options: Partial<Options> = {}) {
    this.AuthorizationFailedError = AuthorizationFailedError;
    this.options = { ...defaultOptions, ...options };
    this.store = new Store(this.options.storage, this.options.storageKey);
  }

  create(options: Partial<Options>) {
    return new SSO(options);
  }

  private waitForLogin(): Promise<object> {
    const _this = this;
    return new Promise((resolve, reject) => {
      // the handler function is named because we have to remove it
      // after we got the message from OUR opened auth wdw/iframe
      wdw?.addEventListener('message', function handler(message) {
        if (message.origin === _this.options.baseUrl) {
          wdw?.removeEventListener('message', handler);
          if (message.data && message.data.access_token) {
            _this.store.set(message.data);
            return resolve(message.data);
          }
          return reject(new AuthorizationFailedError());
        }
      });
    });
  }

  /**
   * Login and get the sso.merl.ooo user
   * @throws AuthorizationFailedError
   */
  async login(): Promise<object> {
    wdw?.open(`${this.options.baseUrl}/${this.options.endpoints.login.url}`, '_blank');

    return this.waitForLogin();
  }

  /**
   * Refresh a sso.merl.ooo login (by calling login page again in hidden iframe)
   * @throws AuthorizationFailedError
   */
  async refreshLogin(): Promise<object> {
    // open login in hidden iframe, and...
    const iframe = document.createElement('iframe');
    document.body.appendChild(iframe);
    iframe.setAttribute('style', 'display: none; width: 0; height: 0;');
    iframe.setAttribute('width', '0');
    iframe.setAttribute('heigth', '0');
    iframe.setAttribute('tabindex', '-1');
    iframe.src= `${this.options.baseUrl}/${this.options.endpoints.login.url}`;

    // ...hope that oauth 'accept' is already set and no user interaction needed
    return pTimeout(this.waitForLogin(), { milliseconds: this.options.timeout })
      .catch(err => {
        if (err instanceof TimeoutError) throw new AuthorizationFailedError('Login timed out');
        throw err;
      })
      .then(data => {
        document.body.removeChild(iframe);
        return data;
      });
  }

  /**
   * Check if user is logged in to sso.merl.ooo
   */
  check({ asynchronous }: { asynchronous: false }): object | null;
  check({ asynchronous }: { asynchronous?: true }): Promise<object | null>;
  check({ asynchronous }: { asynchronous?: boolean } = { asynchronous: true }) {
    const sync = asynchronous !== undefined && !asynchronous;
    const user = this.store.get();

    // check expiry of stored user
    if ((user as any)?.access_token_expires_at * 1000 < Date.now()) this.store.remove();
    else if (user) return sync ? user : Promise.resolve(user);

    function handleError(error: Error) {
      if (error && (error as ApiError).status === 403) return null;
      throw error;
    }

    // check sso cookie, if no stored user
    if (sync) {
      try {
        const data = api(this.options.endpoints.check, this.options, false);
        if (data && data.access_token) {
          this.store.set(data);
          return data;
        }
        return null;
      } catch (error) {
        return handleError(error as Error);
      }
    } else {
      return api(this.options.endpoints.check, this.options)
        .then(({ data }) => {
          if (data && data.access_token) {
            this.store.set(data);
            return data;
          }
          return null;
        })
        .catch(handleError);
    }
  }

  /**
   * Logout from your app (only your app, not sso.merl.ooo entirely)
   */
  async logout(): Promise<void> {
    this.store.remove();
  }

  /**
   * Logout from sso.merl.ooo entirely
   */
  async logoutGlobally(): Promise<void> {
    await api(this.options.endpoints.logout, this.options);
    this.logout();
  }
}

export default new SSO();
