import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import typescript from '@rollup/plugin-typescript';
import clear from 'rollup-plugin-delete';
import dts from 'rollup-plugin-dts';
import nodePolyfills from 'rollup-plugin-polyfill-node';

export default [
  {
    input: 'index.ts',
    plugins: [
      clear({ targets: 'dist' }),
      resolve(),
      nodePolyfills(),
      commonjs(),
      typescript({ tsconfig: './tsconfig.json' }),
    ],
    output: [
      {
        file: 'dist/cjs/index.js',
        format: 'cjs',
        exports: 'default',
        sourcemap: true,
      },
      {
        file: 'dist/esm/index.js',
        format: 'esm',
        exports: 'default',
        sourcemap: true,
      },
    ],
    watch: { exclude: ['node_modules/**'] },
  },
  {
    input: 'dist/esm/index.d.ts',
    plugins: [
      dts(),
      !process.env.ROLLUP_WATCH && clear({ targets: 'dist/*/types', hook: 'buildEnd' }),
    ],
    output: [{ file: 'dist/index.d.ts', format: 'esm' }],
    external: [/\.css$/],
    watch: { exclude: ['node_modules/**'] },
  },
];
